import beaufort from 'beaufort-scale'

const VueBeaufort = {
	// eslint-disable-next-line
	install(Vue, options) {
		if (VueBeaufort.installed) {
			console.warn('It seems like you\'re trying to install vue-beaufort twice.')
			return;
		}
		VueBeaufort.installed = true

		// var beaufort = options && options.beaufort ? options.beaufort : require('beaufort-scale');

		Object.defineProperties(Vue.prototype, {
			$beaufort: {
				get: function () {
					return beaufort;
				}
			}
		});

		Vue.beaufort = beaufort;

		Vue.filter('beaufort', function (value, lang='en', int=false) {
			try {
				return beaufort(value, { lang, int } )
			} catch (err) {
				// Prevent Vue from crashing if incorrect metrics are provided
				// and simply return the original value instead.
				console.error(err);
				return value
			}
		});
	}
}

export default VueBeaufort;
