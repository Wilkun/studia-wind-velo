import Vue from 'vue'
import VueUnits from 'vue-units';
import './plugins/beaufortify'
import './plugins/chartify'
import './plugins/vuetify'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(VueUnits)

new Vue({
  render: h => h(App),
}).$mount('#app')
