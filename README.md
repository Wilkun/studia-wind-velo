# Wind Velocity Converter

## Wymagania 
`node` >= 8.0.0

## Instalacja projektu
```
npm install -g @vue/cli
npm install
```

### Kompilacja i przeładowanie na potrzeby tworzenia oprogramowania
```
npm run serve
```

### Kompilacja i budowanie na produkcję
```
npm run build
```
